#include "kernel.hpp"
#ifdef _AMD
#include <amdlibm.h>
#include <acml.h>
#include <cblas.h>
#endif

#ifdef _MKL
#include "mkl.h"
#include "mkl_lapacke.h"

inline void dgels(char trans, int m, int n, int nrhs, double *a, int lda,
                  double *b, int ldb, int *info) {
  (*info) = LAPACKE_dgels(LAPACK_COL_MAJOR, trans, m, n, nrhs, a, lda,
                          b, ldb);
}
inline void dgelsy(int m, int n, int nrhs, double *a, int lda,
                   double *b, int ldb, int *jpvt, double rcond, int
                   *rank, int *info) {
  (*info) = LAPACKE_dgelsy(LAPACK_COL_MAJOR,m, n, nrhs, a, lda,
                           b, ldb, jpvt, rcond, rank);
  
}
#endif

#include<queue>
using std::priority_queue;

/**  \brief Kernel class -- a fourth order epanechnikov type kernel 
      see gasser, muller, and mammitzsch (1985)
*/
Kernel::Kernel() {
  type = epan;
  order = 2;
}

double Kernel::operator()(const vector<double> &x,const vector<double> &y, double bw,const vector<vector<double> > &R) {
  double k;
  switch (type) {
  case epan: 
    if (order==4) {
      double sumu2 = 0;
      k = 0.46875; //= 15/32
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) u+=R[i][j]*(x[j]-y[j]);
        u/=bw;
        u *= u;
        sumu2 += u;
      }
      if (sumu2<1) k *= (7*sumu2*sumu2-10*sumu2+3)/ipow(bw,x.size());
      else {
        k = 0;          
      }
    } else if (order==2) {
      double sumu2 = 0;
      k = 0.75;
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) u+=R[i][j]*(x[j]-y[j]);
        u/=bw;
        u *= u;
        sumu2 += u;
      }
      if (sumu2<1) k *= (1-sumu2)/ipow(bw,x.size());
      else {
        k = 0;
      }
    } else {
      cout << "Kernel order " << order << " not implemented" << endl;
      exit(1);
    }
    return(k);
  case rbf:
    if (order==2) {
      double r = 0;
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) u+=R[i][j]*(x[j]-y[j]);
        r += u*u;
      }
      r /= (bw*bw);
      k = r<=0? 0:sqrt(r); 
      // other options
      //1:1/(1+r); //0:sqrt(r)*r; //r*log(sqrt(r)); //exp(-r/2)/(bw*sqrt(2*M_PIl)); // sqrt(1+r); //
    } else {
      cout << "Radial Basis Function order " << order << " not implemented" << endl;
      exit(1);
    }
    return(k);
  case gauss2: // gaussian twicing kernel 
    if (order==2) {
      double r = 0;
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) u+=R[i][j]*(x[j]-y[j]);
        r += u*u;
      }
      r /= (bw*bw);
      double k = 2/pow(sqrt(M_PI*2),x.size())*exp(-0.5*r) - 
        1/(sqrt(2)*pow(sqrt(M_PI*2),x.size()))*exp(-0.25*r);
      return(k/bw);
    } else {
      cout << "Gaussian twicing kernel order " << order << " not implemented" << endl;
      exit(1);
    }
  default: 
    cout << "Kernel type not recognized" << endl;
    exit(1);
  } // end switch(type)
  return(0); // can never get here.
}

double Kernel::operator()(const double &x,const double &y, double bw) {
  double k;
  switch (type) {
  case epan: 
    if (order==4) {
      k = 0.46875; //= 15/32
      double u = (x-y);
      u/=bw;
      u *= u;
      if (u<1) k *= 7*u*u-10*u+3;
      else k=0;
    } else if (order==2) {
      k = 0.75;
      double u = (x-y);
      u/=bw;
      u *= u;
      if (u<1) k *= (1-u);
      else k=0;
    } else {
      cout << "Kernel order " << order << " not implemented" << endl;
      exit(1);
    }
    return(k/bw);
  case rbf: 
    if (order==2) {
      printf("Kernel (double,double,double) not implemented for radial basis functions.\n");
      exit(1);
      double u = x-y;
      u /= bw;
      u *= u;
      k = u; //exp(-u/2)/(bw*sqrt(2*M_PIl));
    } else {
      cout << "RBF order " << order << " not implemented" << endl;
      exit(1);
    }
    return(k);      
  default:
    cout << "Kernel type not recognized" << endl;
    exit(1);      
  } // end switch(type)
}

/// returns int_inf^x1 k(x,x2)dx
double Kernel::integrate(double x1, double x2, double bw) {
  double hi = (x1-x2)/bw;
  if (hi>1) return(1.0);
  if (hi<-1) return(0.0);
  if (order==2) {
    return((2-hi*hi*hi+3*hi)/4);
  } else if (order==4) {
    // 15/32*integrate((3-10u^2+7 u^4),u,-1,hi)
    double val = (21*ipow(hi,5)-50*ipow(hi,3)+45*hi+16)/32;
    return(val);
  } else {
    cout << "order " << order << " not implemented" << endl;
    exit(1);
  }
}
  
/// returns int k(x,x1)*k(x,x2) dx
double Kernel::integrate2(double x1,double x2, double bw) {
  double u2 = (x1-x2)/bw;
  double lo = -1<(u2-1)? (u2-1):-1;
  double hi = 1>(u2+1)? (u2+1):1;
  if (lo>=hi) return(0);
  if (order==2) {
    // need integrate((1-((x-x1)/bw)^2)*(1-((x-x2)/bw)^2),x,y1,y2);
    // first make u = (x-x1)/bw
    // let u2 = (x1-x2)/bw
    // so need
    // bw*integrate((1-u^2)*(1-(u-u2)^2),u,lo,hi);
    // used maxima to get this expression
    // 
    double v=(((10*ipow(hi,3)-30*hi)*ipow(u2,2)+(30*ipow(hi,2)-15*ipow(hi,4))*u2+6*ipow(hi,5)-20*ipow(hi,3)+30*hi)/30-
              ((10*ipow(lo,3)-30*lo)*ipow(u2,2)+(30*ipow(lo,2)-15*ipow(lo,4))*u2+6*ipow(lo,5)-20*ipow(lo,3)+30*lo)/30);
    return(v*ipow(0.75,2)/bw);
  } else if (order==4) {
    // substitute as above,
    // used maxima to get this expression
    // integrate((3-10ipow(*u,2)+7ipow(*u,4))*(3-10*(u-uipow(2),2)+7*(u-uipow(2),4)),u,lo,hi);
    double v = 
      ((882*ipow(hi,5)-2100*ipow(hi,3)+1890*hi)*ipow(u2,4)+
       (-2940*ipow(hi,6)+6300*ipow(hi,4)-3780*ipow(hi,2))*ipow(u2,3)+
       (3780*ipow(hi,7)-8820*ipow(hi,5)+6780*ipow(hi,3)-2700*hi)*ipow(u2,2)+
       (-2205*ipow(hi,8)+6300*ipow(hi,6)-6390*ipow(hi,4)+2700*ipow(hi,2))*u2+490*ipow(hi,9)-
       1800*ipow(hi,7)+2556*ipow(hi,5)-1800*ipow(hi,3)+810*hi)/(90)-
      ((882*ipow(lo,5)-2100*ipow(lo,3)+1890*lo)*ipow(u2,4)+
       (-2940*ipow(lo,6)+6300*ipow(lo,4)-3780*ipow(lo,2))*ipow(u2,3)+
       (3780*ipow(lo,7)-8820*ipow(lo,5)+6780*ipow(lo,3)-2700*lo)*ipow(u2,2)+
       (-2205*ipow(lo,8)+6300*ipow(lo,6)-6390*ipow(lo,4)+2700*ipow(lo,2))*u2+490*ipow(lo,9)
       -1800*ipow(lo,7)+2556*ipow(lo,5)-1800*ipow(lo,3)+810*lo)/(90);
    return(v*ipow(0.46875,2)/bw);
  } else {
    cout << "order " << order << " not implemented" << endl;
    exit(1);
  }
}

/// returns the derivative with respect to x[d]
double Kernel::deriv(const vector<double> &x,const vector<double> &y, double bw,const vector<vector<double> > &R, 
                     const size_t d) {    
  double k;
  switch (type) {
  case epan: 
    {         
      double sumu2 = 0;
      double du = 0;
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) { 
          u+=R[i][j]*(x[j]-y[j]);
          du += 2*R[i][j]*(x[j]-y[j])*R[i][d];
        }
        u /= bw;
        u *= u;
        sumu2 += u;
      }
      du /= (bw*bw);
        
      if (order==4) {
        k = 0.46875; //= 15/32
        if (sumu2<1) k *= (2*7*sumu2 - 10)/ipow(bw,x.size()) * du;
        else k = 0;          
      } else if (order==2) {
        k = 0.75;
        if (sumu2<1) k *= -du/ipow(bw,x.size());
        else {
          k = 0;
        }
      } else {
        cout << "Kernel order " << order << " not implemented" << endl;
        exit(1);
      }
      return(k);
    }
  case rbf:
    if (order==2) {
      double r = 0;
      double dr = 0;
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) {
          u+=R[i][j]*(x[j]-y[j]);
          dr += 2*R[i][j]*(x[j]-y[j])*R[i][d];
        }
        r += u*u;
      }
      r /= (bw*bw);
      dr /= (bw*bw);
      k = r<=0? 0:0.5*dr/sqrt(r); 
      // other options
      //1:1/(1+r); //0:sqrt(r)*r; //r*log(sqrt(r)); //exp(-r/2)/(bw*sqrt(2*M_PIl)); // sqrt(1+r); //
    } else {
      cout << "Radial Basis Funciton order " << order << " not implemented" << endl;
      exit(1);
    }
    return(k);
  case gauss2: // gaussian twicing kernel 
    if (order==2) {
      double r = 0;
      double dr = 0;
      for(unsigned int i=0;i<x.size();i++) {
        double u = 0;
        for(unsigned int j=0;j<x.size();j++) {
          u+=R[i][j]*(x[j]-y[j]);
          dr += 2*R[i][j]*(x[j]-y[j])*R[i][d];
        }
        r += u*u;
      }
      r /= (bw*bw);
      dr /= (bw*bw);
      double k = 2/pow(sqrt(M_PI*2),x.size())*exp(-0.5*r)*(-0.5)*dr - 
        1/(sqrt(2)*pow(sqrt(M_PI*2),x.size()))*exp(-0.25*r)*(-0.25)*dr;
      return(k/bw);
    } else {
      cout << "Gaussian twicing kernel order " << order << " not implemented" << endl;
      exit(1);
    }
  default: 
    cout << "Kernel type not recognized" << endl;
    exit(1);
  } // end switch(type)
}


/// Class for finding k nearest neighbors
neighborInfo::neighborInfo() { };
bool operator<(const neighborInfo &a,const neighborInfo &b) {
    return(a.dist<b.dist);
}

/**  \brief Class for approximating policy and value functions

     The class holds the strategy as a function of the state.  A strategy
     is investment as a function of th5Ce state. We approximate strategies using 
     kernel regression.
*/
template<class T>  
T ApproxFn<T>::evalnn(const vector<T> &x) {
  T value;
  unsigned int nn = 10<data.size()? 10:data.size();   
  priority_queue<neighborInfo> neighbor;
  for(unsigned int k=0;k<data.size();k++) {
    neighborInfo candidate;
    candidate.dist = rhol2sq(data[k],x);
    if (k<nn) {
      candidate.val = val[k];
      neighbor.push(candidate);
    } 
    if (candidate.dist<neighbor.top().dist) {
        neighbor.pop();
        candidate.val = val[k];
        neighbor.push(candidate);
    }
  }
  value = 0;
  for (unsigned int k=0;k<nn;k++) {
    if (neighbor.empty()) {
      printf("neighbor is empty with k=%d nn=%d\n",k,nn);
    }
    value += neighbor.top().val;
    neighbor.pop();
  }
  return(value/((double) nn));
}

template<class T>
T ApproxFn<T>::eval(const vector<T> &x) {
  T value;
  T sumw;
  value = 0;
  sumw = 0;
  calls++;
  if (degree == 0) {
    for(unsigned int k=0;k<data.size();k++) {
      //double diff = 0;
      //for(unsigned int j=0;j<x.size() && diff<1e-6;j++) diff += fabs(data[k][j] - x[j]);
      //if (diff < 1e-6) continue; // skip own observation
      double w = kern(data[k],x,bw*mu[k],invRootV);
      sumw += w;
      value += w*val[k];
    }
    if (sumw==0) {      
      neighbor++;        
      return(evalnn(x));
    } 
    if (kern.type!=rbf) return(value/sumw);
    else return(value);
    
    /*}else if (degree == 1) { // local linear
      double *xx;
      double *xy;
      unsigned int nx = x.size() + 1;
      xx = new double[nx*nx];
      xy = new double[nx];
      for (unsigned int j=0;j<nx;j++) {
      xy[j] = 0;
      for (unsigned int k=0;k<nx;k++) xx[j+k*nx] = 0;
      }
      // compute w*x'*x and w*x'*y
      for (unsigned int i=0;i<data.size();i++) {
      double w = kern(x,data[i],bw*mu[i],invRootV);
      for (unsigned int j=0;j<nx;j++) {
      double dxj = (j>0? (x[j-1]-data[i][j-1]):1.0);
      xy[j] += w*dxj*val[i];
      for (unsigned int k=j;k<nx;k++) {
      double dxk = (k>0? (x[k-1]-data[i][k-1]):1.0);
      xx[j+k*nx] += w*dxj*dxk;
      }
      }
      }
      // fill in lower triangle of xx
      for (unsigned int j=0;j<nx;j++) {
      for (unsigned int k=0;k<j;k++) xx[j+k*nx] = xx[k+j*nx];
      }
      // solve for beta
      long info = dgelsy(nx,nx,1,xx,nx,xy,nx);
      if (info) printf("WARNING (ApproxFn::eval) dgelsy info=%d\n",info);
      T value = xy[0];
      delete[] xx;
      delete[] xy;
      return(value);      
    */
  } else if (degree>=1) {  // arbitrary degree
    T value;
    this->deriv(x, 0, value);
    return(value);
  } else {
    printf("ApproxFn::eval() degree=%d not implemented\n",degree);
      exit(degree);
  }
}  

template<class T>
void ApproxFn<T>::evalnnWeights(T *w, const vector<T> &x) {
  const unsigned int nn = 10;   
  priority_queue<neighborInfo> neighbor;
  for(unsigned int k=0;k<data.size();k++) {
    neighborInfo candidate;
    candidate.dist = rhol2sq(data[k],x);
    candidate.index = k;
    if (k<nn) {
      candidate.val = val[k];
      neighbor.push(candidate);
    } 
    if (candidate.dist<neighbor.top().dist) {
      neighbor.pop();
      candidate.val = val[k];
      neighbor.push(candidate);
    }
    w[k] = 0;
  }
  for (unsigned int k=0;k<nn;k++) {
    w[neighbor.top().index] = 1.0/((double) nn);
    neighbor.pop();
  }
}

template<class T>
ApproxFn<T>::ApproxFn() { neighbor = 0; calls = 0; nBad = 0; } ///< default constructor

/** \brief Constructor       
   */
template<class T>
ApproxFn<T>::ApproxFn(double bwin) {    
  bw = bwin;
  neighbor = 0; calls = 0;
  nBad = 0;
  degree = 0;
}

/// returns the function value in a state
template<class T>
T ApproxFn<T>::operator()(State<T> &state) {
  return( this->eval(state.vec()) );
}

template<class T>
T ApproxFn<T>::operator()(const vector<T> &x) {
  return( this->eval(x) );
}

template<class T>
void ApproxFn<T>::evalWeights(T *w, const vector<T> &x) {
  T sumw;
  sumw = 0;
  for(unsigned int k=0;k<data.size();k++) {
    sumw+= (w[k] = kern(data[k],x,bw*mu[k],invRootV));
  }
  if (kern.type==epan) {
    if (sumw==0) {      
      neighbor++;        
      evalnnWeights(w,x);
    } else {
      for(unsigned int k=0;k<data.size();k++) w[k] /= sumw;
    }
  } else { 
    return;
  }
}

template<class T>
ostream& operator<<(ostream& out, const ApproxFn<T>& s) 
{
  out << "Function approximation: \n";
  for(unsigned int k=0;k<s.val.size();k++) {
    out << " val: " << s.val[k] << " data=" << s.data[k] << endl;
  }
  return(out);
}

template<class T>
void ApproxFn<T>::updateData(const vector<vector<double> > &d) {
  vector<double> mean(d[0].size(),0);
  data = d;
  neighbor = 0; calls = 0;  nBad = 0;
  // update invRootV
  // allocate
  invRootV = vector<vector<double> >(data[0].size());
  for(int i=0;i<(int) data[0].size();i++) {
    invRootV[i] = vector<double>(data[0].size(),0);
  }
  // compute variance
  for(int i=0;i<(int) data.size();i++) {
    for(int k=0;k<(int) data[i].size();k++) 
      mean[k] += data[i][k]/data.size();
  }
  for(int i=0;i<(int) data.size();i++) {
    for(int k=0;k<(int) data[i].size();k++) 
      for(int j=0;j<(int) data[i].size();j++)
        invRootV[k][j] += (data[i][k]-mean[k])*(data[i][j]-mean[j])/(data.size()-1);
  }
  // invert and take square root
  //cout << "        V = \n";
  //for(int i = 0; i<(int) invRootV.size(); i++) cout << invRootV[i] << ";\n";
  invRootV = invRootDdet(invRootV);    
  //for(int k=0;k<(int) invRootV.size();k++) 
  //  for(int j=0;j<(int) invRootV.size();j++) invRootV[k][j] = (k==j)? 1/invRootV[k][j]:0;
  //cout << " invRootV = \n";
  //for(int i = 0; i<(int) invRootV.size(); i++) cout << invRootV[i] << ";\n";
  this->updateMu();
}

// derivative with respect to x_d
template<class T>
T ApproxFn<T>::deriv(const vector<T> &x, const size_t d) {
  T value;
  return(this->deriv(x,d,value));
}  

// derivative with respect x_d, also returns value in value
template<class T>
T ApproxFn<T>::deriv(const vector<T> &x, const size_t D, T &value) {
  T sumw;
  T sumdw;
  T dv;
  static unsigned int nwarn = 0;
  if (D>=x.size()) {
    printf("ERROR (ApproxFn::deriv) requested derivative with respect to \n"
           "%dth variable, but x has %d elements\n",(int) D,(int) x.size());
    exit(D);
  }
  if (degree==0) {
    value = 0;
    sumw = 0;
    dv = 0;
    sumdw = 0;
    calls++;
    for(unsigned int k=0;k<data.size();k++) {
      //double diff = 0;
      //for(unsigned int j=0;j<x.size() && diff<1e-6;j++) diff += fabs(data[k][j] - x[j]);
      //if (diff < 1e-6) continue; // skip own observation
      double w  = kern(x,data[k],bw*mu[k],invRootV);
      double dw = kern.deriv(x,data[k],bw*mu[k],invRootV,D);
      sumw += w;
      value += w*val[k];
      dv += dw*val[k];
      sumdw += dw;
    }
    if (sumw==0) {      
      return(0);
    } 
    if (kern.type==epan || kern.type==gauss2) {
      value /= sumw;
      return( (dv - sumdw*value)/sumw );
    }
    else return(dv);
    /*} else if (degree==1) {
      
      double *xx;
      double *xy;
      unsigned int nx = x.size() + 1;
      xx = new double[nx*nx];
      xy = new double[nx];
      for (unsigned int j=0;j<nx;j++) {
        xy[j] = 0;
        for (unsigned int k=0;k<nx;k++) xx[j+k*nx] = 0;
      }
      // compute w*x'*x and w*x'*y
      for (unsigned int i=0;i<data.size();i++) {
        double w = kern(x,data[i],bw*mu[i],invRootV);
        for (unsigned int j=0;j<nx;j++) {
          double dxj = (j>0? (x[j-1]-data[i][j-1]):1.0);
          xy[j] += w*dxj*val[i];
          for (unsigned int k=j;k<nx;k++) {
            double dxk = (k>0? (x[k-1]-data[i][k-1]):1.0);
            xx[j+k*nx] += w*dxj*dxk;
          }
        }
      }
      // fill in lower triangle of xx
      for (unsigned int j=0;j<nx;j++) {
        for (unsigned int k=0;k<j;k++) xx[j+k*nx] = xx[k+j*nx];
      }
      // solve for beta
      long info = dgelsy(nx,nx,1,xx,nx,xy,nx);
      if (info) printf("WARNING (ApproxFn::deriv) dgelsy info=%d\n",info);
      value = xy[0];      
      T dvalue = -xy[1+D];
      delete[] xx;
      delete[] xy;      
      return(dvalue);
      */
  } else { // arbitrary degree (this would also work for degree=1,
    // but was written later, so they're left as separate
    // cases for now)
    double *xx=NULL;
    double *xy=NULL;
    unsigned int nx;
    unsigned int notneeded;
    this->getxwx(&xx,&xy,nx,notneeded, x,0);
    // solve for beta
    int info;
    dgels('N',nx,nx,1,xx,nx,xy,nx,&info);
    if (info) {
      if (nwarn<20) {
        printf("WARNING (ApproxFn::eval) dgels info=%d\n"
                 "A is not full rank. Using dgelsy instead.\n",info);
        nwarn++;
        if (nwarn>=20) {
          printf("Encountered this warning %d times, henceforth suppressing"
                 " it.\n",nwarn);
        }
      }
      double rcond = 1e-12;
      int *jpvt = new int[nx];
      int rank;
      dgelsy(nx,nx,1,xx,nx,xy,nx,jpvt,rcond,&rank,&info);
      delete[] jpvt;
      if (info) {
        printf("ERROR (ApproxFn::eval) dgelsy info=%d\n",info);
        exit(info);
      }
    } 
    value = xy[0];
    T dvalue = -xy[1+D];
    delete[] xx;
    delete[] xy;
    return(dvalue);            
  } 
}  
  
// computes X'W X and X'W Y at point x, where X = matrix of 
//   x - data, W = diag(kernel weights) 
//
// - if onlyxw is nonzero, computes xw (nx by n) instead of xwy (nx
//   by 1)
//
// - xwx and xwy should be unallocated on entry. On return xwx will
//   be size nx by nx and xw will be size nx by n
template<class T>
void ApproxFn<T>::getxwx(double **xwx, double **xwy, unsigned int &nx, 
            unsigned int &N, // output
            const vector<double> &x, const int onlyxw) // input
{ 
  if (onlyxw) N = data.size(); else N = 1;
  if ((*xwx)!=NULL || (*xwy)!=NULL) {
    printf("ERROR ApproxFn::getxwx xwx and xwy must be NULL.\n");
    exit(-1);
  }
  // compute number of terms in polynomial 
  //  number of terms in nx dimension polynomial of degree d = 
  //   sum_p=0^1 (number of combinations with replacement of p elements
  //              from set of size nx)
  //  number combinations with replacement = (n+r-1)! / (r!*(n-1)!)
  unsigned int xs = x.size();
  unsigned int nd1 = xs; // (n+d-1)!/(n-1)!
  unsigned int dfact = 1;
  nx = 1;
  for (int d=1;d<=degree;d++) {
    dfact *= d;
    nd1 *= (xs+d-1);
    nx += nd1/dfact;
  }
  // allocate and initialize to zero
  *xwx = new double[nx*nx];
  *xwy = new double[nx*N]; 
  for (unsigned int i=0;i<nx*nx;i++) (*xwx)[i] = 0;
  for (unsigned int i=0;i<nx*N;i++) (*xwy)[i] = 0;      
  // compute x'wx and x'wy
  for (unsigned int i=0;i<data.size();i++) {
    double w = kern(x,data[i],bw*mu[i],invRootV);
    polyexp dj(xs);
    for (unsigned int j=0;j<nx;j++) {
      polyexp dk(xs);
      double dxj = 1.0;
      for (unsigned int n=0;n<xs;n++) {
        if (dj[n]) dxj *= ipow(x[n]-data[i][n],dj[n]);
      }
      if (onlyxw) (*xwy)[j+i*nx] += w*dxj;
      else (*xwy)[j] += w*dxj*val[i];
      for (unsigned int k=0;k<nx;k++) {
        double dxk = 1.0;
        for (unsigned int n=0;n<xs;n++) {
          if (dk[n]) dxk *= ipow(x[n]-data[i][n],dk[n]);
        }
        (*xwx)[j+k*nx] += w*dxj*dxk;
        dk.next();
      } // k
      dj.next();
    } // j
  } // i
}

// returns derivative*pdf(x)
template<class T>
T ApproxFn<T>::wDeriv(const vector<T> &x, const size_t d) {
  T dv;
  if (kern.type==rbf) {
    printf("ApproxFn::wDeriv() not implemented for RBF\n");
      exit(-1);
  }
  if (degree==0) {
    dv = 0;
    for(unsigned int k=0;k<data.size();k++) {
      double dw = kern.deriv(x,data[k],bw*mu[k],invRootV,d);
      dv += dw*val[k];
    }
    return(dv/data.size());
  } else { //if (degree>=1) {
    double w = 0;
    for(unsigned int k=0;k<data.size();k++) w += kern(x,data[k],bw*mu[k],invRootV);
    w/=data.size();
    return(w*this->deriv(x,d));
  } 
}

template<class T>
void ApproxFn<T>::updateMu() {
  // update mu
  vector<double> fi(data.size());
  double den = 0;
  int n=0;
  mu = vector<double>(data.size(),1.0);
  if (kern.type==rbf) return;
  double minfi = HUGE_VAL; // smallest fi | fi>0
  for(int i=0;i<(int) data.size();i++) {
    fi[i] = 0;
    for(int k=0;k<(int) data.size();k++) {
      if (k!=i) fi[i] += kern(data[k],data[i],bw,invRootV);
    }
    fi[i] /= (data.size()-1);
    fi[i] = fabs(fi[i]);
    if (fi[i]>0) {
      n++; 
      den += log(fi[i]); 
      if (fi[i]<minfi) minfi = fi[i];
    }
  }
  for(int i=0;i<(int) data.size();i++) { 
    if (fi[i]>0) mu[i] = pow(fi[i]/exp(den/n),-1.0/(3.0+2*data[0].size()));    
    else mu[i] = pow(minfi/exp(den/n),-1.0/(3.0+2*data[0].size()));
  }        
}

/* Bandwidth selection: Li and Racine (2008) - no automatic way for cdf, so use pdf cv. */
/**  
     cross validation objective function for bandwidth selection
     see Hall, Li, and Racine (2004) -- min of cvDens is optimal bandwidth 
     for f(val|data) 
     
     warning: modifies this->mu to match bw
*/
template<class T>
double ApproxFn<T>::cvDens(double bw, double bwv) {
  //double bwv = bw;
  double cv = 0;                             
  double sdval = 1, meanval=0;
  vector<double> xk(data[0].size()), xj(data[0].size());
  vector<double> Kint(data.size()*data.size(),1);
  vector<double> kernij(data.size()*data.size(),0);
  //this->updateMu();
  for(unsigned int j=0;j<val.size();j++)  meanval += val[j];
  meanval /= val.size();
  sdval = 0;
  for(unsigned int j=0;j<val.size();j++)  sdval += (val[j]-meanval)*(val[j]-meanval);
  sdval = sqrt(sdval/val.size());
#pragma omp parallel default(shared)
  {
    //double *work = new double[stateSolve.size()];
#pragma omp for 
    for(int j=0;j<(int) data.size();j++) {
      for(int k=j;k<(int) data.size();k++) {
        Kint[j+k*data.size()] = kern.integrate2(val[j],val[k],bwv);
        if (j!=k) Kint[k+j*data.size()] = Kint[j+k*data.size()];
        kernij[j+k*data.size()] = kern(data[j],data[k],bw*mu[j],invRootV);
        if (j!=k) kernij[k+j*data.size()] = kern(data[k],data[j],bw*mu[k],invRootV);
      }
    }
    
#pragma omp for reduction(+ : cv)
    for(int i=0;i<(int) data.size();i++) {
      double Gi=0, gi=0, mui=0;
      for(int j=0;j<(int) data.size();j++) {
        double kxji = kernij[i+j*data.size()];
        if (j==i) continue;
        for(int k=j;k<(int) data.size();k++) {
          if (k==i) continue;
          double kxki = kernij[i+k*data.size()];
          if (k==j) Gi += kxki*kxji*Kint[j+k*data.size()];
          else Gi += 2*kxki*kxji*Kint[j+k*data.size()];
        }
        gi += kxji*kern(val[i],val[j],bwv);
        mui += kxji;
      }
      if (mui==0) {
        mui = 1e-16; 
      }
      cv += Gi/(mui*mui) - 2*gi/mui;
    }
  } // end parallel region
  cv /= (data.size());
  return(cv);
}  
  
/* cv for regression */
template<class T>
double ApproxFn<T>::cvReg(double bw) {
  double cv = 0;
  double minv = val[0], maxv=val[0];
  for(size_t i=0;i<val.size();i++) { 
    minv=val[i]<minv?val[i]:minv; 
    maxv=val[i]>maxv?val[i]:maxv;
  }
  for(size_t i=0;i<val.size();i++) {
    double valmi=0;
    double den=0;
    for(size_t k=0;k<val.size();k++) {
      if (k==i) continue;
      double w = kern(data[i],data[k],mu[k]*bw,invRootV);
      den += w;
        valmi += w*val[k];
    }
    if (den==0) cv += (maxv-minv)*(maxv-minv);
    else cv += (val[i] - valmi/den)*(val[i]-valmi/den);
  }
  return(cv/data.size());
}


// for debugging: eval with info printed
template<class T>
T ApproxFn<T>::evalVerbose(const vector<T> &x) {
  T value;
  T sumw;
  T sumw2=0;
  value = 0;
  sumw = 0;
#pragma omp critical 
  { nBad++;  }
  printf("data.size()=%d\n",(int) data.size());
  cout << "x = " << x << endl;
  
  for(unsigned int k=0;k<data.size();k++) {
    double w = kern(x,data[k],bw*mu[k],invRootV);
    sumw2 += w;
  }
  for(unsigned int k=0;k<data.size();k++) {
    double w = kern(x,data[k],bw*mu[k],invRootV);
    if (w!=0) {
      //cout << k << ":" << data[k];
      printf(" | mu[%d]=%6.3g w=%6.3g, val=%6.3g, w/sumw=%6.3g\n",k,mu[k],w,val[k],w/sumw2);
      cout << data[k] << endl;
    }
    sumw += w;
    value += w*val[k];
  }
  if (sumw==0) {      
    neighbor++;        
    return(evalnn(x));
  } 
  printf(" value=%6.3g, sumw=%6.3g, value/sumw=%6.3g\n",value,sumw,value/sumw);
  return(value/sumw);
}  

template class ApproxFn<double>;

/** objective function wrapper for using nlopt for minimization
    in bandwidth selection h*/
double cvObjective(int n, const double *x, double *grad, void *appFn) 
{
  grad = NULL;
  return(((ApproxFn<double> *) appFn)->cvDens(x[0],x[1]));
}

double cvObjReg(int n, const double *x, double *grad, void *appFn)
{
  grad = NULL;
  return(((ApproxFn<double> *) appFn)->cvReg(x[0]));
}


  
