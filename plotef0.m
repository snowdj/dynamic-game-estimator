clear; close all;
%graphics_toolkit('fltk');
graphics_toolkit('gnuplot');
%close all;
%[output txt] = system('make results');
%display(txt);
%if (output) 
%  error('failed running game.exe');
%end
dat = csvread('efunc0.csv');
s = dat(:,1);
v = dat(:,2);
dv = dat(:,3);
k = dat(:,4);
ok = dat(:,5);
e = dat(:,end-4);
prof = dat(:,end-3);
evhat = dat(:,end-2);
vt = dat(:,end-1);
dvt = dat(:,end);
name = '';
oklab = 'x';


%vt = (vt- prof)/0.9;
%v = (v - prof)/0.9;
fprintf('corr(v,vt) = %.2f, spearman(v,vt) =  %.2f\n', ...
        corr(v,vt),spearman(v,vt));
fprintf('corr(vt,prof) = %.2f, spearman(vt,prof)=%.2f\n', ...
        corr(vt,prof), spearman(vt,prof));
x = [v ones(numel(v),1)];
bv = (x'*x) \ x'*vt;
x = [vt ones(numel(v),1)];
bvt = (x'*x) \ x'*v;
fprintf('(v''*v)\\ v''*vt = %.3f, (vt''*vt)\\ vt''*v=%.3f\n', ...
        bv(1),bvt(1));
figure;
bv = v(v>max(vt) | v<min(vt));
bvt = vt(v>max(vt) | v<min(vt));
bv(bv>max(vt)) = max(vt);
bv(bv<min(vt)) = min(vt);
plot(vt,v,'.',[min(vt) max(vt)],[min(vt),max(vt)],'-',bvt,bv,'rx')
xlabel('True Evalue');
ylabel('Estimated Evalue');
%axis([min(vt) max(vt) min(vt),max(vt)]);

figure;
bv = dv(dv>max(dvt) | dv<min(dvt));
bvt = dvt(dv>max(dvt) | dv<min(dvt));
bv(bv>max(dvt)) = max(dvt);
bv(bv<min(dvt)) = min(dvt);
plot(dvt,dv,'.',[min(dvt) max(dvt)],[min(dvt),max(dvt)],'-',bvt,bv,'rx')
xlabel('True dEvalue');
ylabel('Estimated dEvalue');
axis([min(dvt) max(dvt) min(dvt)-1,max(dvt)+1]);

%stop
if (0)
  figure;
  scatter3(k,ok,s,5,e)
  xlabel('own capital')
  ylabel(oklab)
  %ylabel('eta')
  zlabel('invest');
  title(['policy function' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(1) = gcf;
end

dat = dat(1:400,:);
s = dat(:,1);
v = dat(:,2);
dv = dat(:,3);
k = dat(:,4);
ok = dat(:,5);
e = dat(:,end-4);
prof = dat(:,end-3);
evhat = dat(:,end-2);
vt = dat(:,end-1);
dvt = dat(:,end);

figure;
scatter3(k,ok,v,5,e)
xlabel('capital')
ylabel(oklab);
%ylabel('eta')
zlabel('value');
title(['value function' name]);
%colorbar();
set(gca,'YDir','reverse');
fh(2) = gcf;
  
figure;
scatter3(k,ok,vt,5,e)
xlabel('capital')
ylabel(oklab);
%ylabel('eta')
zlabel('value');
title(['value function true' name]);
%colorbar();
set(gca,'YDir','reverse');
%goodaxis = axis();
fh(3) = gcf;    

%figure(2);
%axis(goodaxis);
quantile(v,0:0.1:1)
quantile(vt,0:0.1:1)
mean((v-vt).^2)

figure;
scatter3(k,ok,v-vt,5,e)
xlabel('capital')
ylabel(oklab);
%ylabel('eta')
zlabel('value');
title(['value function error' name]);
%colorbar();
set(gca,'YDir','reverse');
fh(4) = gcf;    

figure;
scatter3(k,ok,dvt,5,e)
xlabel('capital')
ylabel(oklab);
%ylabel('eta')
zlabel('dvalue');
title(['dvalue function true' name]);
%colorbar();
set(gca,'YDir','reverse');
goodaxis = axis();
fh(3) = gcf;    

figure;
scatter3(k,ok,dv,5,e)
xlabel('capital')
ylabel(oklab);
%ylabel('eta')
zlabel('value');
title(['dvalue function' name]);
%colorbar();
set(gca,'YDir','reverse');
axis(goodaxis);
fh(2) = gcf;

%figure(2);
[quantile(dv,0:0.1:1) quantile(dvt,0:0.1:1)]
mean((dv-dvt).^2)

figure;
scatter3(k,ok,dv-dvt,5,e)
xlabel('capital')
ylabel(oklab);
%ylabel('eta')
zlabel('dvalue');
title(['dvalue function error' name]);
%colorbar();
set(gca,'YDir','reverse');
fh(4) = gcf;    

if (0)
  figure; 
  scatter3(k,ok,prof,5,e)
  xlabel('capital')
  ylabel(oklab);
  zlabel('value');
  title(['profit ' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(5) = gcf;    
  
  figure; 
  scatter3(k,ok,vt-prof,5,e)
  xlabel('capital')
  ylabel(oklab);
  zlabel('value');
  title(['ev true ' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(6) = gcf;    
  
  figure; 
  scatter3(k,ok,v-prof,5,e)
  xlabel('capital')
  ylabel(oklab);
  zlabel('value');
  title(['ev est ' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(7) = gcf;    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = csvread('data.csv');
profit = data(:,3);
id = unique(data(:,1));
time = unique(data(:,2));
N = numel(id);
T = numel(time);
profit = reshape(profit,T,N);
vobs = (.9.^(0:(T-1))*profit)';
vest0 = data(data(:,2)==0,4);
vt0 = data(data(:,2)==0,5);
group = floor(id/(N/4));
for g=0:3
  fprintf('%d: %6.3g %6.3g %6.3g\n',g, ...
          mean(vobs(group==g)), ...
          mean(vest0(group==g)), ...
          mean(vt0(group==g)));
end
