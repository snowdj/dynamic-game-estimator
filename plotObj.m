clear;
close all;
dat = csvread('plotObj.csv');
c = abs(dat(:,3) - 0.2)<1e-4;
x = dat(c,1);
y = dat(c,2);
objval = dat(c,5);
nx = numel(unique(x));
ny = numel(unique(y));
figure;
x = reshape(x,nx,ny);
y = reshape(y,nx,ny);
objval = reshape(objval,nx,ny);
surf(x,y,objval);
xlabel('psi0');
ylabel('psi1');
title('conditional on psi2=0.2');

c = abs(dat(:,1) - 0.2)<1e-4;
x = dat(c,2);
y = dat(c,3);
objval = dat(c,5);
nx = numel(unique(x));
ny = numel(unique(y));
figure;
x = reshape(x,nx,ny);
y = reshape(y,nx,ny);
objval = reshape(objval,nx,ny);
surf(x,y,objval);
xlabel('psi1');
ylabel('psi2');
title('conditional on psi0=0.2');

c = abs(dat(:,2) - 0.1)<1e-4;
x = dat(c,1);
y = dat(c,3);
objval = dat(c,5);
nx = numel(unique(x));
ny = numel(unique(y));
figure;
x = reshape(x,nx,ny);
y = reshape(y,nx,ny);
objval = reshape(objval,nx,ny);
surf(x,y,objval);
xlabel('psi0');
ylabel('psi2');
title('conditional on psi1=0.1');


%foo = objval;
%foo(foo>10) = NaN;
%figure;
%surf(x,y,foo);
