function [fh] = plotf(filename,name)
  dat = csvread(filename);
  s = dat(:,1);
  ss = dat(:,2);
  v = dat(:,3);
  vs = dat(:,4);
  mu = dat(:,5);
  k = dat(:,6);
  ok = dat(:,7);
  e = dat(:,end);
  if (size(dat,2)>8)    
    oklab = 'other capital';
  else 
    oklab = 'x';
  end
  
  figure;
  scatter3(k,ok,s,5,e)
  xlabel('own capital')
  ylabel(oklab)
  %ylabel('eta')
  zlabel('invest');
  title(['policy function' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(1) = gcf;
  
  figure;
  scatter3(k,ok,v,5,e)
  xlabel('capital')
  ylabel(oklab);
  %ylabel('eta')
  zlabel('value');
  title(['value function' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(2) = gcf;

  figure; 
  plot(e,s,'.',e,ss,'.');
  legend('policy','br');
  xlabel('eta');
  ylabel('invest');
  title('policy and br');
  fh(3) = gcf;

  figure; 
  plot(e,v,'.',e,vs,'.');
  legend('value','value(br)');
  xlabel('eta');
  ylabel('value');
  title('value and valur(br)');
  fh(4) = gcf;
  
  figure;
  plot(s,s,'r:',s,ss,'b.');
  xlabel('Approximate policy function');
  ylabel('Best response to approximate policy function');
  r=axis();
  axis([r(3) r(4) r(3) r(4)]);
  fh(5) = gcf;

  figure;
  plot(v,v,'r:',v,vs,'b.');
  xlabel('Approximate value function');
  ylabel('Value of best response to approximation');  
  fh(6) = gcf;

  %figure; 
  %scatter3(e,s,ss,5,mu);
  %set(gca,'YDir','reverse');
  %xlabel('eta');
  %ylabel('invest');
  %zlabel('br');
  %fh(7) = gcf;
  
end


