#include "misc.hpp"

/** 
    Output operator for vectors.
*/
template<class T>
ostream& operator<<(ostream& out, const vector<T>& v) {
  if (v.size() > 0) {
    for(unsigned int i=0;i<(v.size()-1);i++) out << v[i] << " , ";
    out << v[v.size()-1];
  }
  return(out);
}

template ostream& operator<< <double>(ostream&, const vector<double> &);
/* ******************************************************************************** */
/** Exponents for a multivariate polynomial ordered as (in 3d)
    0,0,0
    1,0,0
    0,1,0
    0,0,1
    2,0,0
    1,1,0 
    etc
    Used for local polynomial regression
*/
polyexp::polyexp() {}
polyexp::polyexp(int dim) { // set to dim dimensions and 0
  p = vector<int>(dim,0);
}

void polyexp::reset() { // reset to zero
  p = vector<int>(p.size(),0);
}
// accessors
vector<int> polyexp::operator()() {
  return(p);
}
int polyexp::operator[](int j) { return(p[j]); }
// increment
vector<int> polyexp::next() {
  int i=p.size()-2;
  for(i=(p.size()-2);i>=0;i--) {
    if (p[i]) { // shift right
      p[i]--;
      p[i+1]++;
      return(p);
    }
  } 
  p[0] = p[p.size()-1] + 1;
  p[p.size()-1] = 0;
  return(p);
}

/* ******************************************************************************** */
  
/** \brief State class 
    
    A state is the quality of each firm, x, and epsilon 
*/
template<class T>
State<T>::State() { }

/// Constructo
template<class T>
State<T>::State(T ownQ, vector<double> q, vector<double> xin, double etain) {
  ownQuality = ownQ;
  otherQuality = q;
  x = xin;
  eta = etain;
}    

template<class T>
const vector<T> & State<T>::vec() {
  v.resize(otherQuality.size()+x.size()+2);
  unsigned int i;
  v[0] = ownQuality;
  for(i=1;i<=otherQuality.size();i++) {
    v[i] = otherQuality[i-1];
  }
  for(unsigned int j=0;j<x.size();j++) v[i++] = x[j];
  v[i++] = eta;
  return(v);
}

template<class T>
void State<T>::unvec(vector<T> vec) {
  int i =0;
  ownQuality = vec[i++];
  for(unsigned int j=0;j<otherQuality.size();j++) {
    otherQuality[j] = vec[i++];
  }
  for(unsigned int j=0;j<x.size();j++) x[j]=vec[i++];
  eta = vec[i++];
}

template class State<double>;
/* ******************************************************************************** */
