clear;
close all;
% settings
font = 'VarelaRound-Regular';
fh=plotf('func.csv','');

%% value function plot
figure(fh(2));
colormap('copper')
axes1 = gca;
set(axes1,'FontName',font);
view(axes1,[-59.5, 22])
%box(axes1,'on');
grid(axes1,'on');
hold(axes1,'all');
xlabel('w');
ylabel('k');
zlabel('Value');
title('');
% label colorbar
%annotation(gcf,'textbox', ... %[1 0.5 0 0], ...
%           [0.901010101010101 0.484179407176287 0.0373737373737374 0.046801872074883],...
%           'String',{'\eta'},'FontName',font, 'LineStyle','none');
print('-depsc2','figures/value');
system('epstopdf figures/value.eps -o=figures/value.pdf');
print('-dpng','test.png')
%colorbar('peer',axes1,'XaxisLocation','top','Fontname',font);

%% policy function plot
figure(fh(1));
colormap('copper')
axes1 = gca;
set(axes1,'FontName',font);
view(axes1,[-59.5, 22])
%box(axes1,'on');
grid(axes1,'on');
hold(axes1,'all');
xlabel('w');
ylabel('k');
zlabel('Invest');
title('');
print('-depsc2','figures/policy');
system('epstopdf figures/policy.eps -o=figures/policy.pdf');

%% value function error
figure(fh(6));
colormap('copper');
axes1 = gca;
set(axes1,'FontName',font);
xlabel('Approximate value function');
ylabel('Value of best response to approximation');
title('');
print('-depsc2','figures/valError');
system('epstopdf figures/valError.eps -o=figures/valError.pdf');

%% policy function error
figure(fh(5));
colormap('copper');
axes1 = gca;
set(axes1,'FontName',font);
xlabel('Approximate policy function');
ylabel('Best response to approximation');
title('');
print('-depsc2','figures/polError');
system('epstopdf figures/polError.eps -o=figures/polError.pdf');

