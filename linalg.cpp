#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <numeric>
#include <set>
#include <queue>
#include <string>
#include <cstdlib>

#include <time.h>
#include <sys/time.h>
#include <omp.h>
#include <math.h>
#include <assert.h>
#include <string.h>

#include "magma.h"

extern "C" {
#include "random.h"
}
#include <nlopt.h>
#include "linalg.h" // linear algebra and other basic functions

#ifdef _AMD
#include <acml.h> // AMD's lapack 
#endif

#ifdef _MKL
#include <mkl.h>
#include <mkl_lapacke.h>
#endif

// MAGMA interface
long dgelsMagma(int m, int n, int nrhs, double* a, int lda,
                       double* b, int ldb)
{ // could be bad to call cuda twice at same time with same
  // context. not entirely sure how cuda works, but just to be safe 
  magma_int_t info;
  int lwork;
  double *gpuA, *gpuB;
  double *work, workopt;
  //char trans = 'N';
  magma_trans_t trans = MagmaNoTrans;
  static bool initialized = false;
  if (!initialized) {
    magma_init();
    if( CUBLAS_STATUS_SUCCESS != cublasInit( ) ) {                      
      std::cerr << "CUBLAS: Failed to initialize" << std::endl;
      magma_finalize();
      exit(-1);           
    }                                                                   
    magma_print_environment();
    initialized=true;
  } 
  
  lwork = -1;
  if (CUBLAS_STATUS_SUCCESS !=
      cublasAlloc(m*n,sizeof(double),(void**)&gpuA)) 
    printf("Failure 1\n");
  if (CUBLAS_STATUS_SUCCESS !=
      cublasAlloc(ldb*nrhs,sizeof(double),(void**)&gpuB)) 
    printf("Failure 2\n");
  cublasSetMatrix(m,n,sizeof(double),a,lda,gpuA,lda);  
  cublasSetMatrix(ldb,nrhs,sizeof(double),b,ldb,gpuB,ldb);
  magma_dgels3_gpu(trans,(magma_int_t) m,(magma_int_t) n, (magma_int_t) nrhs, 
                   gpuA, (magma_int_t) lda, gpuB, (magma_int_t) ldb, 
                   &workopt, lwork, &info);
  if (info!=0) {
    printf("ERROR: magma_dgels_gpu returned %d\n",info);
    exit(info);
  }
  lwork = (int) workopt;
  work = new double[lwork];
  magma_dgels3_gpu(trans,(magma_int_t) m,(magma_int_t) n, (magma_int_t) nrhs, 
                   gpuA, (magma_int_t) lda, gpuB, (magma_int_t) ldb, 
                     work, lwork, &info);
  if (info!=0) {
    printf("ERROR: magma_dgels_gpu returned %d\n",info);
    exit(info);
  }
  cublasGetMatrix(m,n,sizeof(double),gpuA,lda,a,lda);
  cublasGetMatrix(ldb,nrhs,sizeof(double),gpuB,ldb,b,ldb);
  delete[] work;
  cublasFree(gpuB);
  cublasFree(gpuA);
  return info;
}
 
/// invert a matrix
void invert(double *A, int n) 
{
int info, *ipiv;
  ipiv = new int[n];
#ifdef _MKL
  info = LAPACKE_dgetrf(LAPACK_COL_MAJOR, n, n, A, n, ipiv);
#else
  dgetrf(n,n,A,n,ipiv,&info);
#endif
  for(int i=0;i<n;i++) std::cout << ipiv[i] << " ";
  //std::cout << std::endl;
  if (info!=0) std::cout << "dgetrf returned " << info << std::endl;
#ifdef _MKL
  info = LAPACKE_dgetri(LAPACK_COL_MAJOR, n, A, n, ipiv);
#else
  dgetri(n,A,n,ipiv,&info);
#endif
  if (info!=0) std::cout << "dgetri returned " << info << std::endl;
  delete[] ipiv;
}


/// take (A)^{-1/2} for sym psd matrix a and return det(A)
double invRoot(double *A, int n) 
{
  int info;
  double *eigval, *eigvec;
  double det = 1;
  eigval = new double[n];
  eigvec = new double[n*n];
  memcpy(eigvec,A,n*n*sizeof(double));
#ifdef _MKL
  info = LAPACKE_dsyev(LAPACK_COL_MAJOR,'V','U',n,eigvec,n,eigval);
#else
  dsyev('V','U',n,eigvec,n,eigval,&info);
#endif
  for(int i=0;i<n;i++) det *= eigval[i];
  for(int i=0;i<n;i++) {
    for(int j=0;j<n;j++) {
      A[i*n+j] = 0;
      for(int k=0;k<n;k++) {
        A[i*n+j] += eigvec[k*n+i]*eigvec[k*n+j]/sqrt(eigval[k]);
      }
    }
  }
  delete[] eigval;
  delete[] eigvec;
  return(det);
}

double invnormcdf(double p,const double mu,const double var)
{ //Produces the normal deviate Z corresponding to a given lower tail area of P; Z is accurate to about 1 part in 10**16.
    double q,r,inv;
    double SPLIT1 = 0.425,SPLIT2 = 5.0,CONST1 = 0.180625,CONST2 = 1.6;
    // Coefficients for P close to 0.5
    double A0=3.3871328727963666080,A1=1.3314166789178437745e+2,A2=1.9715909503065514427e+3;
    double A3=1.3731693765509461125e+4,A4=4.5921953931549871457e+4,A5=6.7265770927008700853e+4;
    double A6=3.3430575583588128105e+4,A7=2.5090809287301226727e+3,B1=4.2313330701600911252e+1;
    double B2=6.8718700749205790830e+2,B3=5.3941960214247511077e+3,B4=2.1213794301586595867e+4;
    double B5=3.9307895800092710610e+4,B6=2.8729085735721942674e+4,B7=5.2264952788528545610e+3;
    // Coefficients for P not close to 0, 0.5 or 1.
    double C0=1.42343711074968357734,C1=4.63033784615654529590,C2=5.76949722146069140550;
    double C3=3.64784832476320460504,C4=1.27045825245236838258,C5=2.41780725177450611770e-1;
    double C6=2.27238449892691845833e-2,C7=7.74545014278341407640e-4,D1=2.05319162663775882187;
    double D2=1.67638483018380384940,D3=6.89767334985100004550e-1,D4=1.48103976427480074590e-1;
    double D5=1.51986665636164571966e-2,D6=5.47593808499534494600e-4,D7=1.05075007164441684324e-9;
    // Coefficients for P near 0 or 1.
    double E0=6.65790464350110377720,E1=5.46378491116411436990,E2=1.78482653991729133580;
    double E3=2.96560571828504891230e-1,E4=2.65321895265761230930e-2,E5=1.24266094738807843860e-3;
    double E6=2.71155556874348757815e-5,E7=2.01033439929228813265e-7,F1=5.99832206555887937690e-1;
    double F2=1.36929880922735805310e-1,F3=1.48753612908506148525e-2,F4=7.86869131145613259100e-4;
    double F5=1.84631831751005468180e-5,F6=1.42151175831644588870e-7,F7=2.04426310338993978564e-15;
    if (p<1e-5) return(mu-4.27*sqrt(var));
    else if (p>1-1e-5) return(mu+4.27*sqrt(var));
    q=p-0.5;
    if (fabs(q)<SPLIT1) {
	r = CONST1 - (q*q);
	inv=q*(((((((A7*r+A6)*r+A5)*r+A4)*r+A3)*r+A2)*r+A1)*r+A0)/(((((((B7*r+B6)*r+B5)*r+B4)*r+B3)*r+B2)*r+B1)*r+1.0);
	inv*=sqrt(var);
	inv+=mu;
	return(inv);
    }
    else {
	r = q<0.0 ? p:1.0-p;
	r=sqrt(-log(r));
	if (r<SPLIT2) {
	    r-=CONST2;
	    inv=(((((((C7*r+C6)*r+C5)*r+C4)*r+C3)*r+C2)*r+C1)*r+C0)/(((((((D7*r+D6)*r+D5)*r+D4)*r+D3)*r+D2)*r+D1)*r+1.0);
	}
	else {
	    r-=SPLIT2;
	    inv=(((((((E7*r+E6)*r+E5)*r+E4)*r+E3)*r+E2)*r+E1)*r+E0)/(((((((F7*r+F6)*r+F5)*r+F4)*r+F3)*r+F2)*r+F1)*r+1.0);
	}
	if (q<0.0) inv=-inv;
	inv*=sqrt(var);
	inv+=mu;
	return(inv);
    }
}

// returns A^{-1/2}*det(A)^{1/(2n)}, so the return value as det=1
using namespace std;
vector<vector<double> > invRootDdet(const vector<vector<double> > &A) {
  double *a,det;
  vector<vector<double> > iroota=A;
  int n = A.size();
  a = new double[n*n];
  for(int i=0;i<n;i++) {
    for(int j=0;j<n;j++) {
      a[i*n+j] = A[i][j];
    }
  }
  det=invRoot(a,n);
  det = pow(det,1.0/(2.0*n));
  for(int i=0;i<n;i++) {
    for(int j=0;j<n;j++) {
      iroota[i][j]=a[i*n+j]*det;
    }
  } 
  delete[] a;
  return(iroota);
}

double rhoL2(const vector<double> &x,const vector<double> &y)
{
  double d=0;
  //  if (x.size() != y.size()) coutexit(1);
  for(size_t i=0;i<x.size();i++) { d+= (x[i]-y[i])*(x[i]-y[i]); }
  return(sqrt(d/x.size()));
}

double rhol2sq(const vector<double> &x,const vector<double> &y)
{
  double d=0;
  //  if (x.size() != y.size()) coutexit(1);
  for(size_t i=0;i<x.size();i++) { d+= (x[i]-y[i])*(x[i]-y[i]); }
  return(d);
}

double rhol2(const vector<double> &x,const vector<double> &y)
{
  return(sqrt(rhol2sq(x,y)));
}


void nloptReturnCodeMessage(int returnCode) {
  if (returnCode>=0) {
    printf("nlopt succeeded with code %d: ",returnCode);
    switch(returnCode) {
    case 1: printf("generic success value.\n"); break;
    case 2: printf("objective<=stopval.\n"); break;
    case 3: printf("ftol_rel or ftol_abs reached.\n"); break;
    case 4: printf("xtol_rel or xtol_abs reached.\n"); break;
    case 5: printf("maxeval reached.\n"); break;
    case 6: printf("maximum time reached.\n"); break;
    default: printf("unrecognized code.\n"); 
    }
  } else {
    printf("nlopt failed with code %d: ", returnCode);
    switch(returnCode) {
    case -1: printf("generic failure code.\n"); break;
    case -2: printf("invalid arguments.\n"); break;
    case -3: printf("out of memory.\n"); break;
    case -4: printf("roundoff errors limited progress.\n"); break;
    case -5: printf("forced termination.\n"); break;
    default: printf("unrecognized code.\n"); 
    }
  }
}

/* +++Date last modified: 05-Jul-1997 */

/*
**  IPOW.C - Raise a number to an integer power
**
**  public domain by Mark Stephen with suggestions by Keiichi Nakasato
*/

double ipow(double x, int n)                      /* return x^n */
{
  double t = 1.0;
  
  if (!n)
    return 1.0;                               /* At the top. 0**0 = 1
                                               */
  if (n < 0)
    {
      n = -n;
      x = 1.0/x;                                /* error if x ==
                                                   0. Good                        */
    }                                             /* ZTC/SC returns
                                                     inf, which is
                                                     even better     */
  if (x == 0.0)
    return 0.0;
  do
    {
      if (n & 1)
        t *= x;
      n /= 2;                                   /* KN prefers if
                                                   (n/=2) x*=x; This
                                                   avoids an    */
      x *= x;                                   /* unnecessary but
                                                   benign
                                                   multiplication on     */
    } while (n);                                  /* the last pass,
                                                     but the
                                                     comparison is
                                                     always  */
  return t;                                     /* true _except_ on
                                                   the last pass.              */
}
